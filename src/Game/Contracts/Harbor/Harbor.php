<?php

namespace BinaryStudioAcademy\Game\Contracts\Harbor;

interface Harbor
{
    public function createShip(int $reduce);
}