<?php

namespace BinaryStudioAcademy\Game\Contracts\Player;

interface Command
{
    public function execute();
}