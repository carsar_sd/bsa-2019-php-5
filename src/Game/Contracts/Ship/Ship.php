<?php

namespace BinaryStudioAcademy\Game\Contracts\Ship;

use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

interface Ship
{
    public function Feature();
    public function Coords();
    public function Swim(string $direction, array $harbors);
    public function Buy(string $product);
    public function Fight(array $harbors, Random $random);
    public function Maraud(array $harbors);
}
