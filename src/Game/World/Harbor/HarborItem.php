<?php

namespace BinaryStudioAcademy\Game\World\Harbor;

use BinaryStudioAcademy\Game\Contracts\Harbor\Harbor;
use BinaryStudioAcademy\Game\World\Ship\ShipItem;

class HarborItem implements Harbor
{
    private $harborName;
    private $harborId;
    private $shipType;
    private $coords;
    private $ship;

    public function __construct(string $harborName, int $harborId, string $shipType, array $coords)
    {
        $this->harborName = $harborName;
        $this->harborId   = $harborId;
        $this->shipType   = $shipType;
        $this->coords     = $coords;
    }

    public function createShip(int $reduce = 0)
    {
        return $this->ship = new ShipItem($this->harborId, $this->shipType, $this->coords, $reduce);
    }

    public function getHarborStat($harbor_id)
    {
        $harborData = "Harbor {$this->harborId}: {$this->harborName}." . PHP_EOL;

        if ($harbor_id != $this->harborId) {
            $harborData .= "You see {$this->ship->getName()}: " . PHP_EOL
            . "strength: " . $this->ship->getStrength() . PHP_EOL
            . "armour: " . $this->ship->getArmour() . PHP_EOL
            . "luck: " . $this->ship->getLuck() . PHP_EOL
            . "health: " . $this->ship->getHealth()  . PHP_EOL;
        }

        return $harborData;
    }

    public function getName()
    {
        return $this->harborName;
    }

    public function getId()
    {
        return $this->harborId;
    }

    public function getShipType()
    {
        return $this->shipType;
    }

    public function getCoords()
    {
        return $this->coords;
    }

    public function getShip()
    {
        return $this->ship;
    }
}
