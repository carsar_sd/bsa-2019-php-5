<?php

namespace BinaryStudioAcademy\Game\World\Harbor;

use BinaryStudioAcademy\Game\Contracts\Harbor\Harbor;

class HarborFactory
{
    public function createHarbor(string $harborName, int $harborId, string $shipType, array $coords): Harbor
    {
        return new HarborItem($harborName, $harborId, $shipType, $coords);
    }
}
