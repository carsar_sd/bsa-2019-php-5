<?php

namespace BinaryStudioAcademy\Game\World\Ship;

use BinaryStudioAcademy\Game\Contracts\Ship\Ship;

class ShipFactory
{
    public function createShip(int $harborId, string $shipType, array $coords, int $reduce): Ship
    {
        return new ShipItem($harborId, $shipType, $coords, $reduce);
    }
}
