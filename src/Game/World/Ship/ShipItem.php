<?php

namespace BinaryStudioAcademy\Game\World\Ship;

use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Ship\Ship;
use BinaryStudioAcademy\Game\Exceptions\ErrorCode;
use BinaryStudioAcademy\Game\Exceptions\GameException;
use BinaryStudioAcademy\Game\Helpers\Config;
use BinaryStudioAcademy\Game\Helpers\Math;

class ShipItem implements Ship
{
    private $harborId;
    private $coords;
    private $name;
    private $strength;
    private $health;
    private $armour;
    private $luck;
    private $hold;

    public function __construct(int $harborId, string $shipType, array $coords, int $reduce)
    {
        $shipData = Config::SHIPS[$shipType];

        $this->harborId = $harborId;
        $this->coords   = $coords;
        $this->name     = $shipData['name'];
        $this->strength = $shipData['stats']['strength'] - $reduce;
        $this->armour   = $shipData['stats']['armour'] - $reduce;
        $this->luck     = $shipData['stats']['luck'] - $reduce;
        $this->health   = $shipData['stats']['health'];
        $this->hold     = $shipData['stats']['hold'];
    }

    public function Feature()
    {
        return 'Ship stats:' . PHP_EOL
            . 'strength: ' . $this->strength . PHP_EOL
            . 'armour: ' . $this->armour . PHP_EOL
            . 'luck: ' . $this->luck . PHP_EOL
            . 'health: ' . $this->health . PHP_EOL
            . 'hold: ' . Config::HOLD_TYPES[$this->hold['gold'] . '' . $this->hold['rum']] . PHP_EOL;
    }

    public function Coords()
    {
        if ($harbor_id = array_search($this->coords, Config::HARBORS_COORDS)) {
            $msg = "Harbor {$harbor_id}: ".Config::HARBORS[$harbor_id]['harbor'] . PHP_EOL . PHP_EOL;
        } else {
            $msg = "You are in the open sea. Your coords are: x:{$this->coords['x']}, y:{$this->coords['y']}" . PHP_EOL . PHP_EOL;
        }

        $coords = Config::HARBORS_COORDS;
        $coords[1] = $this->getCoords();

        $pic = '+--+--+--+--+--+--+--+--+--+' . PHP_EOL;
        for ($i = 6; $i > 0; $i--) {
            for ($j = 1; $j < 11; $j++) {
                $harbor_id = array_search(['x' => $j, 'y' => $i], $coords);
                $point = $harbor_id ? Config::HARBORS[$harbor_id]['point'] : '  ';
                $pic .= '|' . $point;

                if ($j == 10) {
                    $pic .= PHP_EOL;
                }
            }
        }
        $pic .= '+--+--+--+--+--+--+--+--+--+' . PHP_EOL;

        return $msg . $pic;
    }

    public function Swim(string $direction, array $harbors)
    {
        $method = 'move' . ucfirst($direction);

        return $this->$method($harbors);
    }

    public function Buy(string $product)
    {
        if ($this->coords == Config::HARBORS_COORDS[$this->harborId]) {
            if ($this->hold['gold'] != 0) {
                if ($product == 'rum') {
                    if (array_sum($this->hold) <= Config::SHIP_HOLD_MAX) {
                        $this->hold['rum'] += 1;
                        $this->hold['gold'] -= 1;
                        return "You\'ve bought a rum. Your hold contains {$this->hold['rum']} bottle(s) of rum." . PHP_EOL;
                    } else {
                        return GameException::interpretException(ErrorCode::HOLD_IS_FULL);
                    }
                } else {
                    $this->$product += 1;
                    $this->hold['gold'] -= 1;
                    return "You\'ve bought a {$product}. Your {$product} is {$this->$product}." . PHP_EOL;
                }
            } else {
                return GameException::interpretException(ErrorCode::NOT_ENOUGH_GOLD);
            }
        } else {
            return GameException::interpretException(ErrorCode::WRONG_HARBOR_STORE);
        }
    }

    public function Blooze()
    {
        if ($this->hold['rum'] != 0) {
            $this->health += 30;
            $this->hold['rum'] -= 1;

            return "You\'ve drunk a rum and your health filled to {$this->health}";
        } else {
            return GameException::interpretException(ErrorCode::RUM_IS_ABSENT);
        }
    }

    public function Fight(array $harbors, Random $random)
    {
        $harbor_id = array_search($this->coords, Config::HARBORS_COORDS);

        if ($harbor_id && $harbor_id != $this->harborId) {
            $player = $this;
            $enemy  = $harbors[$harbor_id]->getShip();
            $calc   = new Math();

            $fighting = true;

            $playerHealth = $player->getHealth();
            $playerLuck   = $calc->luck($random, $player->getLuck());
            $playerDamage = $calc->damage($player->getStrength(), $player->getArmour());

            $enemyHealth  = $enemy->getHealth();
            $enemyLuck    = $calc->luck($random, $enemy->getLuck());
            $enemyDamage  = $calc->damage($enemy->getStrength(), $enemy->getArmour());

            // player fire
            if ($fighting && $enemyHealth > 0) {
                $enemyHealth = $enemyHealth + $enemyLuck - $playerDamage;
                $enemy->setHealth($enemyHealth ?: 0);

                if ($enemyHealth <= 0) {
                    if ($harbor_id == Config::FINAL_DESTINATION) {
                        $message = '🎉🎉🎉Congratulations🎉🎉🎉' . PHP_EOL
                            . '💰💰💰 All gold and rum of Great Britain belong to you! 🍾🍾🍾';

                        return ['exit' => true, 'message' => $message];
                    } else {
                        return ['message' => "{$enemy->name} on fire. Take it to the boarding." . PHP_EOL];
                    }
                }
            } else {
                return ['message' => GameException::interpretException(ErrorCode::YOU_ALREADY_WIN)];
            }

            // enemy fire
            if ($fighting && $playerHealth > 0) {
                $playerHealth = $playerHealth + $playerLuck - $enemyDamage;
                $player->setHealth($playerHealth ?: 0);

                if ($playerHealth <= 0) {
                    $message = "Your ship has been sunk." . PHP_EOL
                        . "You restored in the Pirate Harbor." . PHP_EOL
                        . "You lost all your possessions and 1 of each stats." . PHP_EOL;

                    return ['recreate' => true, 'message' => $message];

                }
            } else {
                return ['message' => GameException::interpretException(ErrorCode::ENEMY_ALREADY_WIN)];
            }

            return ['message' => "{$player->name} has damaged on: {$playerDamage} points." . PHP_EOL
                . "health: {$playerHealth}" . PHP_EOL
                . "{$enemy->name} damaged your ship on: {$enemyDamage} points." . PHP_EOL
                . "health: {$enemyHealth}" . PHP_EOL];

        } else {
            return ['message' => GameException::interpretException(ErrorCode::NO_ENEMY)];
        }
    }

    public function Maraud(array $harbors)
    {
        $harbor_id = array_search($this->coords, Config::HARBORS_COORDS);

        if ($harbor_id && $harbor_id != $this->harborId) {
            $player = $this;
            $enemy  = $harbors[$harbor_id]->getShip();

            if ($enemy->getHealth() <= 0) {
                $playerHold = $player->getHold();
                $enemyHold  = $enemy->getHold();
                $playerHoldTotal = array_sum($playerHold);
                $enemyHoldTotal  = array_sum($enemyHold);

                if ($enemyHoldTotal > 0) {
                    if ($playerHoldTotal + $enemyHoldTotal <= Config::SHIP_HOLD_MAX) {
                        $totalHold = [
                            'gold' => $playerHold['gold'] + $enemyHold['gold'],
                            'rum' => $playerHold['rum'] + $enemyHold['rum']
                        ];
                        $player->setHold($totalHold);

                        return $enemyHold['gold'] ? 'You got 💰.' . PHP_EOL : 'You got 🍾.' . PHP_EOL;
                    } else {
                        return GameException::interpretException(ErrorCode::HOLD_IS_FULL);
                    }
                } else {
                    return GameException::interpretException(ErrorCode::ENEMY_HOLD_EMPTY);
                }
            } else {
                return GameException::interpretException(ErrorCode::STILL_ALIVE);
            }
        } else {
            return GameException::interpretException(ErrorCode::NO_ENEMY);
        }
    }

    private function moveNorth($harbors)
    {
        if ($this->coords['y'] < Config::WORLD_COORDS_MAX['y']) {
            $intersect = [];

            foreach ($harbors as $id => $harbor) {
                $harbor_coords = $harbor->getCoords();

                if ($this->coords['x'] == $harbor_coords['x'] && $harbor_coords['y'] > $this->coords['y']) {
                    $intersect[$id] = $harbor_coords['y'];
                }
            }

            if (!empty($intersect)) {
                asort($intersect);
                $harbor_id  = key($intersect);
                $harbor     = $harbors[$harbor_id];
                $harborData = $this->moveToHarbor($harbor_id, $harbor);

                return $harborData;
            } else {
                $this->coords['y'] = Config::WORLD_COORDS_MAX['y'];
                return "Your current coords are: x:{$this->coords['x']}, y:{$this->coords['y']}" . PHP_EOL;
            }
        } else {
            return GameException::interpretException(ErrorCode::LIMIT_OF_DIRECTION, 'north');
        }
    }

    private function moveSouth($harbors)
    {
        if ($this->coords['y'] > 1) {
            $intersect = [];

            foreach ($harbors as $id => $harbor) {
                $harbor_coords = $harbor->getCoords();

                if ($this->coords['x'] == $harbor_coords['x'] && $harbor_coords['y'] < $this->coords['y']) {
                    $intersect[$id] = $harbor_coords['y'];
                }
            }

            if (!empty($intersect)) {
                arsort($intersect);
                $harbor_id  = key($intersect);
                $harbor     = $harbors[$harbor_id];
                $harborData = $this->moveToHarbor($harbor_id, $harbor);

                return $harborData;
            } else {
                $this->coords['y'] = 1;
                return "Your current coords are: x:{$this->coords['x']}, y:{$this->coords['y']}" . PHP_EOL;
            }
        } else {
            return GameException::interpretException(ErrorCode::LIMIT_OF_DIRECTION, 'south');
        }
    }

    private function moveWest($harbors) // <-
    {
        if ($this->coords['x'] > 1) {
            $intersect = [];

            foreach ($harbors as $id => $harbor) {
                $harbor_coords = $harbor->getCoords();

                if ($this->coords['y'] == $harbor_coords['y'] && $harbor_coords['x'] < $this->coords['x']) {
                    $intersect[$id] = $harbor_coords['x'];
                }
            }

            if (!empty($intersect)) {
                arsort($intersect);
                $harbor_id  = key($intersect);
                $harbor     = $harbors[$harbor_id];
                $harborData = $this->moveToHarbor($harbor_id, $harbor);

                return $harborData;
            } else {
                $this->coords['x'] = 1;
                return "Your current coords are: x:{$this->coords['x']}, y:{$this->coords['y']}" . PHP_EOL;
            }
        } else {
            return GameException::interpretException(ErrorCode::LIMIT_OF_DIRECTION, 'west');
        }
    }

    private function moveEast($harbors) // ->
    {
        if ($this->coords['x'] < Config::WORLD_COORDS_MAX['x']) {
            $intersect = [];

            foreach ($harbors as $id => $harbor) {
                $harbor_coords = $harbor->getCoords();

                if ($this->coords['y'] == $harbor_coords['y'] && $harbor_coords['x'] > $this->coords['x']) {
                    $intersect[$id] = $harbor_coords['x'];
                }
            }

            if (!empty($intersect)) {
                asort($intersect);
                $harbor_id  = key($intersect);
                $harbor     = $harbors[$harbor_id];
                $harborData = $this->moveToHarbor($harbor_id, $harbor);

                return $harborData;
            } else {
                $this->coords['x'] = Config::WORLD_COORDS_MAX['x'];
                return "Your current coords are: x:{$this->coords['x']}, y:{$this->coords['y']}" . PHP_EOL;
            }
        } else {
            return GameException::interpretException(ErrorCode::LIMIT_OF_DIRECTION, 'east');
        }
    }

    private function moveToHarbor($harbor_id, $harbor)
    {
        $this->setCoords($harbor->getCoords());

        /** -- create new ship **/
        if ($harbor_id != $this->harborId) {
            $harbor->createShip();
        }
        /** // create new ship **/

        $harborData = $harbor->getHarborStat($this->harborId);

        /** -- health reparetion **/
        $healthMin = Config::PLAYER_MIN_HEALTH;

        if ($harbor_id == $this->harborId && $this->health < $healthMin) {
            $this->health = $healthMin;
            $harborData .= "Your health is repared to {$healthMin}." . PHP_EOL;
        }
        /** // health reparetion **/

        return $harborData;
    }

    public function getHarborId()
    {
        return $this->harborId;
    }

    public function getCoords()
    {
        return $this->coords;
    }

    private function setCoords($coords)
    {
        $this->coords = $coords;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStrength()
    {
        return $this->strength;
    }

    private function setStrength($strength)
    {
        $this->strength = $strength;
    }

    public function getArmour()
    {
        return $this->armour;
    }

    private function setArmour($armour)
    {
        $this->armour = $armour;
    }

    public function getLuck()
    {
        return $this->luck;
    }

    private function setLuck($luck)
    {
        $this->luck = $luck;
    }

    public function getHealth()
    {
        return $this->health;
    }

    private function setHealth($health)
    {
        $this->health = $health;
    }

    public function getHold()
    {
        return $this->hold;
    }

    private function setHold($hold)
    {
        $this->hold = $hold;
    }
}
