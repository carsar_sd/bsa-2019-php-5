<?php

namespace BinaryStudioAcademy\Game\Exceptions;

class GameException
{
    public static function interpretException(string $exception, string $message = ''): string
    {
        switch ($exception) {
            case ErrorCode::INCORRECT_DIRECTION:
                return "Harbor not found in this direction";
            case ErrorCode::LIMIT_OF_DIRECTION:
                return "Your ship has been already reached the end of the world on the " . ucfirst($message) . "!";
            case ErrorCode::HARBOR_ABOARD:
                return "There is no ship to aboard";
            case ErrorCode::HARBOR_FIRE:
                return "There is no ship to fight";
            case ErrorCode::ABOARD_SHIP:
                return "You cannot board this ship, since it has not yet sunk";
            case ErrorCode::INCORRECT_DIRECTION_PARAM:
                return "Direction '{$message}' incorrect, choose from: east, west, north, south";
            case ErrorCode::INCORRECT_GOOD_PARAM:
                return "Good '{$message}' incorrect, choose from: strength, armour, luck, rum";
            case ErrorCode::UNKNOWN_COMMAND:
                return "Command '{$message}' not found";
            case ErrorCode::WRONG_HARBOR_STORE:
                return "You can shop only in your own harbor!";
            case ErrorCode::NOT_ENOUGH_GOLD:
                return "You do not have enough gold to make a purchase!";
            case ErrorCode::HOLD_IS_FULL:
                return "Your hold hasn't enough free place!";
            case ErrorCode::RUM_IS_ABSENT:
                return "You don't have rum in your hold!";
            case ErrorCode::NO_ENEMY:
                return "There is no enemy here!";
            case ErrorCode::YOU_ALREADY_WIN:
                return "You have already won! Take the ship to the boarding.";
            case ErrorCode::ENEMY_ALREADY_WIN:
                return "You have already sunk!";
            case ErrorCode::STILL_ALIVE:
                return "Your enemy has been alive yet!";
            case ErrorCode::ENEMY_HOLD_EMPTY:
                return "Enemy hold is empty!";
            default:
                return "Shit happens! " . $message;
        }
    }
}
