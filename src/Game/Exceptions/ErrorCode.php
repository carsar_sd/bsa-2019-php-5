<?php

namespace BinaryStudioAcademy\Game\Exceptions;

final class ErrorCode
{
    public const INCORRECT_DIRECTION = 'incorrect_direction';
    public const INCORRECT_DIRECTION_PARAM = 'incorrect_direction_command';
    public const LIMIT_OF_DIRECTION = 'limit_of_direction';
    public const INCORRECT_GOOD_PARAM = 'incorrect_good_command';
    public const HARBOR_ABOARD = 'pirate_harbor_aboard';
    public const HARBOR_FIRE = 'pirate_harbor_fire';
    public const ABOARD_SHIP = 'aboard_live_ship';
    public const UNKNOWN_COMMAND = 'unknown_command';
    public const UNKNOWN_ERROR = 'unknown_error';
    public const WRONG_HARBOR_STORE = 'wrong_harbor_store';
    public const NOT_ENOUGH_GOLD = 'not_enough_gold';
    public const HOLD_IS_FULL = 'hold_is_full';
    public const RUM_IS_ABSENT = 'rum_is_absent';
    public const NO_ENEMY = 'no_enemy';
    public const YOU_ALREADY_WIN = 'you_already_win';
    public const ENEMY_ALREADY_WIN = 'enemy_already_win';
    public const STILL_ALIVE = 'still_alive';
    public const ENEMY_HOLD_EMPTY = 'enemy_hold_empty';
    public const EMPTY_PARAM = [
        'set-sail' => self::INCORRECT_DIRECTION_PARAM, 'buy' => self::INCORRECT_GOOD_PARAM
    ];
}
