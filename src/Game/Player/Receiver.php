<?php

namespace BinaryStudioAcademy\Game\Player;

use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Helpers\Config;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Exceptions\ErrorCode;
use BinaryStudioAcademy\Game\Exceptions\GameException;

class Receiver
{
    private $writer;
    private $game;

    public function __construct(Writer $writer, Game $game)
    {
        $this->writer = $writer;
        $this->game = $game;
    }

    public function debug():void
    {
        echo PHP_EOL; print_r($this->game); echo PHP_EOL;
    }

    /**
     * implementation of help command
     *
     * @return void
     */
    public function help():void
    {
        $helpData = "List of commands:" . PHP_EOL
        . "help - shows this list of commands" . PHP_EOL
        . "stats - shows stats of ship" . PHP_EOL
        . "set-sail <east|west|north|south> - moves in given direction" . PHP_EOL
        . "fire - attacks enemy's ship" . PHP_EOL
        . "aboard - collect loot from the ship" . PHP_EOL
        . "buy <strength|armour|luck|rum> - buys skill or rum: 1 chest of gold - 1 item" . PHP_EOL
        . "drink - your captain drinks 1 bottle of rum and fill 30 points of health" . PHP_EOL
        . "whereami - shows current harbor" . PHP_EOL
        . "exit - finishes the game";

        $this->writer->writeln($helpData);
    }

    /**
     * implementation of stats command
     *
     * @return void
     */
    public function stats():void
    {
        $statsData = $this->game->player->Feature();

        $this->writer->writeln($statsData);
    }

    /**
     * implementation of whereami command
     *
     * @return void
     */
    public function whereami():void
    {
        $coordsData = $this->game->player->Coords();

        $this->writer->writeln($coordsData);
    }

    /**
     * implementation of set-sail command
     *
     * @param string $param
     * @return void
     */
    public function setsail($param):void
    {
        if (!$param || !in_array($param, Config::DIRECTIONS)) {
            $this->writer->writeln(GameException::interpretException(ErrorCode::INCORRECT_DIRECTION_PARAM, $param));
            return;
        }

        $sailData = $this->game->player->Swim($param, $this->game->harbors);

        $this->writer->writeln($sailData);
    }

    /**
     * implementation of buy command
     *
     * @param string $param
     * @return void
     */
    public function buy($param):void
    {
        if (!$param || !in_array($param, Config::GOODS)) {
            $this->writer->writeln(GameException::interpretException(ErrorCode::INCORRECT_GOOD_PARAM, $param));
            return;
        }

        $buyData = $this->game->player->Buy($param);

        $this->writer->writeln($buyData);
    }


    /**
     * implementation of drink command
     *
     * @return void
     */
    public function drink():void
    {
        $drinkData = $this->game->player->Blooze();

        $this->writer->writeln($drinkData);
    }

    /**
     * implementation of fire command
     *
     * @return void
     */
    public function fire():void
    {
        $fireData = $this->game->player->Fight($this->game->harbors, $this->game->random);

        if (isset($fireData['recreate'])) {
            $harborId = $this->game->player->getHarborId();
            $this->game->player = $this->game->harbors[$harborId]->createShip(1);
        }

        if (isset($fireData['exit'])) {
            $this->game->exit = true;
        }

        $this->writer->writeln($fireData['message']);
    }

    /**
     * implementation of aboard command
     *
     * @return void
     */
    public function aboard():void
    {
        $aboardData = $this->game->player->Maraud($this->game->harbors);

        $this->writer->writeln($aboardData);
    }
}
