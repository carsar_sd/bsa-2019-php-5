<?php

namespace BinaryStudioAcademy\Game\Player;

use BinaryStudioAcademy\Game\Contracts\Player\Command;

class debugCommand implements Command
{
    private $receiver;

    public function __construct(Receiver $receiver) {
        $this->receiver = $receiver;
    }

    public function execute() {
        $this->receiver->debug();
    }
}

class helpCommand implements Command
{
    private $receiver;

    public function __construct(Receiver $receiver) {
        $this->receiver = $receiver;
    }

    public function execute() {
        $this->receiver->help();
    }
}

class statsCommand implements Command
{
    private $receiver;

    public function __construct(Receiver $receiver) {
        $this->receiver = $receiver;
    }

    public function execute() {
        $this->receiver->stats();
    }
}

class setsailCommand implements Command
{
    private $param;
    private $receiver;

    public function __construct(Receiver $receiver, string $param) {
        $this->param = $param;
        $this->receiver = $receiver;
    }

    public function execute() {
        $this->receiver->setsail($this->param);
    }
}

class fireCommand implements Command
{
    private $receiver;

    public function __construct(Receiver $receiver) {
        $this->receiver = $receiver;
    }

    public function execute() {
        $this->receiver->fire();
    }
}

class aboardCommand implements Command
{
    private $receiver;

    public function __construct(Receiver $receiver) {
        $this->receiver = $receiver;
    }

    public function execute() {
        $this->receiver->aboard();
    }
}

class buyCommand implements Command
{
    private $param;
    private $receiver;

    public function __construct(Receiver $receiver, string $param) {
        $this->param = $param;
        $this->receiver = $receiver;
    }

    public function execute() {
        $this->receiver->buy($this->param);
    }
}

class drinkCommand implements Command
{
    private $receiver;

    public function __construct(Receiver $receiver) {
        $this->receiver = $receiver;
    }

    public function execute() {
        $this->receiver->drink();
    }
}

class whereamiCommand implements Command
{
    private $receiver;

    public function __construct(Receiver $receiver) {
        $this->receiver = $receiver;
    }

    public function execute() {
        $this->receiver->whereami();
    }
}
