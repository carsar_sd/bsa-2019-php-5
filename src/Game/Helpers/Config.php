<?php

namespace BinaryStudioAcademy\Game\Helpers;

final class Config
{
    public const GOODS = [
        'strength',
        'armour',
        'luck',
        'rum'
    ];
    public const DIRECTIONS = [
        'south',
        'west',
        'north',
        'east'
    ];

    public const PLAYER_MIN_HEALTH = 60;

    public const EXIT_COMMAND = 'exit';
    public const EXIT_MESSAGE = 'Good bye!';

    public const COMMAND_COUNT = 2;
    public const COMMAND_CLASS = 'BinaryStudioAcademy\Game\Player\\';

    public const COMMANDS_HAS_PARAMS = [
        'set-sail',
        'buy'
    ];

    public const SHIP_HOLD_MAX = 3;
    public const HOLD_TYPES = [
        '00' => '[ _ _ _ ]',
        '10' => '[ _ _ 💰 ]',
        '01' => '[ _ _ 🍾 ]',
        '11' => '[ _ 💰 🍾 ]',
        '02' => '[ _ 🍾 🍾 ]',
        '20' => '[ _ 💰 💰 ]',
        '03' => '[ 🍾 🍾 🍾 ]',
        '12' => '[ 💰 🍾 🍾 ]',
        '21' => '[ 💰 💰 🍾 ]',
        '30' => '[ 💰 💰 💰 ]',
    ];

    public const FINAL_DESTINATION = 8;
    public const WORLD_COORDS_MAX = ['x' => 9, 'y' => 6];
    public const HARBORS_COORDS = [
        1 => ['x' => 4, 'y' => 3],
        2 => ['x' => 4, 'y' => 1],
        3 => ['x' => 1, 'y' => 4],
        4 => ['x' => 4, 'y' => 6],
        5 => ['x' => 7, 'y' => 6],
        6 => ['x' => 9, 'y' => 5],
        7 => ['x' => 7, 'y' => 2],
        8 => ['x' => 9, 'y' => 2],
    ];
    public const HARBORS = [
        1 => [
            'harbor' => 'Pirates Harbor',
            'ship'   => 'player',
            'coords' => self::HARBORS_COORDS[1],
            'point'  => '💀',
        ],
        2 => [
            'harbor' => 'Southhampton',
            'ship'   => 'schooner',
            'coords' => self::HARBORS_COORDS[2],
            'point'  => 'Sh',
        ],
        3 => [
            'harbor' => 'Fishguard',
            'ship'   => 'schooner',
            'coords' => self::HARBORS_COORDS[3],
            'point'  => 'Sh',
        ],
        4 => [
            'harbor' => 'Salt End',
            'ship'   => 'schooner',
            'coords' => self::HARBORS_COORDS[4],
            'point'  => 'Sh',
        ],
        5 => [
            'harbor' => 'Isle of Grain',
            'ship'   => 'schooner',
            'coords' => self::HARBORS_COORDS[5],
            'point'  => 'Sh',
        ],
        6 => [
            'harbor' => 'Grays',
            'ship'   => 'battle',
            'coords' => self::HARBORS_COORDS[6],
            'point'  => 'Bt',
        ],
        7 => [
            'harbor' => 'Felixstowe',
            'ship'   => 'battle',
            'coords' => self::HARBORS_COORDS[7],
            'point'  => 'Bt',
        ],
        8 => [
            'harbor' => 'London Docks',
            'ship'   => 'royal',
            'coords' => self::HARBORS_COORDS[8],
            'point'  => 'Ro',
        ]
    ];
    public const SHIPS = [
        'player' => [
            'name'  => 'Jolly Roger',
            'stats' => [
                'strength' => 4,
                'armour'   => 4,
                'luck'     => 4,
                'health'   => 60,
                'hold'     => ['gold' => 0, 'rum' => 0],
            ]
        ],
        'schooner' => [
            'name'  => 'Royal Patrool Schooner',
            'stats' => [
                'strength' => 4,
                'armour'   => 4,
                'luck'     => 4,
                'health'   => 50,
                'hold'     => ['gold' => 1, 'rum' => 0],
            ]
        ],
        'battle' => [
            'name'  => 'Royal Battle Ship',
            'stats' => [
                'strength' => 8,
                'armour'   => 8,
                'luck'     => 7,
                'health'   => 80,
                'hold'     => ['gold' => 0, 'rum' => 1],
            ]
        ],
        'royal' => [
            'name'  => 'HMS Royal Sovereign',
            'stats' => [
                'strength' => 10,
                'armour'   => 10,
                'luck'     => 10,
                'health'   => 100,
                'hold'     => ['gold' => 2, 'rum' => 1],
            ]
        ]
    ];
}
