<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Helpers\Config;
use BinaryStudioAcademy\Game\Player\Receiver;
use BinaryStudioAcademy\Game\Exceptions\ErrorCode;
use BinaryStudioAcademy\Game\Exceptions\GameException;
use BinaryStudioAcademy\Game\World\Harbor\HarborFactory;

class Game
{
    public $random;
    public $harbors;
    public $player;
    public $exit = false;

    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln('');
        $writer->writeln('******************************');
        $writer->writeln('* Welcome to "Pirates" game! *');
        $writer->writeln('******************************');
        $writer->writeln('*  Press enter to start...   *');
        $reader->read();
        $writer->writeln('Adventure has begun. Wish you good luck!');

        $writer->writeln('');
        $this->createWorld();
        $writer->writeln('World has been created! Enjoy!');

        $pic = '+--+--+--+--+--+--+--+--+--+' . PHP_EOL;
        for ($i = 6; $i > 0; $i--) {
            for ($j = 1; $j < 11; $j++) {
                $harbor_id = array_search(['x' => $j, 'y' => $i], Config::HARBORS_COORDS);
                $point = $harbor_id ? Config::HARBORS[$harbor_id]['point'] : '  ';
                $pic .= '|' . $point;

                if ($j == 10) {
                    $pic .= PHP_EOL;
                }
            }
        }
        $pic .= '+--+--+--+--+--+--+--+--+--+';
        $writer->writeln($pic);

        $writer->writeln('');
        $writer->writeln('[:: waiting a command ::]');
        $writer->writeln('');

        while (($input = $reader->read()) != Config::EXIT_COMMAND) {
            $this->processing($writer, $input);

            if ($this->exit) {
                break;
            } else {
                $writer->writeln('');
                $writer->writeln('[:: next command ::]');
                $writer->writeln('');
            }
        }

        $writer->writeln(Config::EXIT_MESSAGE);
    }

    public function run(Reader $reader, Writer $writer)
    {
        $this->createWorld();
        $input = $reader->read();
        $this->processing($writer, $input);
    }

    /**
     * Check and execute player command
     *
     * @param Writer $writer
     * @param string $playerInput
     * @return void
     */
    public function processing(Writer $writer, $playerInput):void
    {
        $parseInput = explode(' ', $playerInput);

        if (count($parseInput) > Config::COMMAND_COUNT) {
            $writer->writeln(GameException::interpretException(ErrorCode::UNKNOWN_COMMAND, $playerInput));
            return;
        }

        $parseInput = array_map('mb_strtolower', $parseInput);

        $inputCommand = $parseInput[0];
        $commandParam = $parseInput[1] ?? '';

        if ($commandParam && !in_array($inputCommand, Config::COMMANDS_HAS_PARAMS)) {
            $writer->writeln(GameException::interpretException(ErrorCode::UNKNOWN_COMMAND, $playerInput));
            return;
        } elseif (!$commandParam && in_array($inputCommand, Config::COMMANDS_HAS_PARAMS)) {
            $writer->writeln(GameException::interpretException(ErrorCode::EMPTY_PARAM[$inputCommand], $playerInput));
            return;
        }

        $commandClass = Config::COMMAND_CLASS . preg_replace('/[^a-z]/i', '', $inputCommand) . 'Command';

        if (class_exists($commandClass)) {
            try {
                $args    = $commandParam ? [new Receiver($writer, $this), $commandParam] : [new Receiver($writer, $this)];
                $command = new $commandClass(...$args);
                $command->execute();
            } catch (\Exception $e) {
                $writer->writeln(GameException::interpretException(ErrorCode::UNKNOWN_ERROR, $e->getMessage()));
                return;
            }
        } else {
            $writer->writeln(GameException::interpretException(ErrorCode::UNKNOWN_COMMAND, $playerInput));
            return;
        }
    }

    public function createWorld()
    {
        $this->harbors = [];
        $HarborFactory = new HarborFactory();

        foreach (Config::HARBORS as $id => $harbor) {
            $this->harbors[$id] = $HarborFactory->createHarbor($harbor['harbor'], $id, $harbor['ship'], $harbor['coords']);
        }

        $this->player = $this->harbors[1]->createShip();
    }
}
